package com.example.demo.pdf;

import java.awt.Color;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.springframework.stereotype.Component;

import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;

@Component
public class PdfService {

	public void pdf() throws Exception {

		Document document = new Document(PageSize.A4);

		OutputStream os = new FileOutputStream("C:\\Users\\Anugna\\Revision\\new.pdf");

		PdfWriter.getInstance(document, os);

		document.open();
		Font fontTitle = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		fontTitle.setSize(18);

		Paragraph paragraph = new Paragraph("Antra", fontTitle);
		paragraph.setAlignment(Paragraph.ALIGN_CENTER);

		Font fontPara = FontFactory.getFont(FontFactory.TIMES_ROMAN);
		fontPara.setSize(12);

		Paragraph body = new Paragraph("Welcome to Pdf Generator", fontPara);
		body.setAlignment(Paragraph.ALIGN_JUSTIFIED);

		document.add(paragraph);
		document.add(body);
		document.close();
	}

	public void pdfTable() throws Exception {

		Document document = new Document(PageSize.A4);

		OutputStream os = new FileOutputStream("C:\\Users\\Anugna\\Revision\\new1.pdf");

		PdfWriter.getInstance(document, os);

		document.open();

		Table table = new Table(4, 3);
		table.setBorder(4);

		table.setBorderColor(Color.BLUE);

		table.setAlignment(Table.ALIGN_CENTER);

		table.addCell("Id");
		table.addCell("Name");
		table.addCell("Age");
		table.addCell("Role");

		table.addCell("123");
		table.addCell("Sivakanth");
		table.addCell("26");
		table.addCell("Java Developer");

		table.addCell("143");
		table.addCell("Priya");
		table.addCell("22");
		table.addCell("Actor");

		document.add(table);
		document.close();
	}
}