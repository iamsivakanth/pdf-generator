package com.example.demo.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.example.demo.pdf.PdfService;

@Component
public class Runner implements CommandLineRunner {

	@Autowired
	PdfService pdfService;

	@Override
	public void run(String... args) throws Exception {

		pdfService.pdfTable();
	}
}